package client;

import database.GUIBase;
import model.Kwadrat;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.IOException;


/**
 * Created by Łukasz on 2016-03-19.
 */
public class PlanszaGryClient extends JFrame {
    private final DataOutputStream dataOutputStream;
    private Kwadrat[][] kwadraty = new Kwadrat[3][3];

    public PlanszaGryClient(DataOutputStream dataOutputStream) {

        this.dataOutputStream = dataOutputStream;
        init();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                kwadraty[i][j] = new Kwadrat(i, j);
                final int finalJ = j;
                final int finalI = i;
                kwadraty[i][j].addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                            wyslijDoSerwera(String.valueOf(finalI) + " " + String.valueOf(finalJ));
                            kwadraty[finalI][finalJ].ustawKolko();
                    }
                });
                this.add(kwadraty[i][j]);
            }
        }
    }

    private void init() {
        this.setTitle("Gracz 2 - Klient");
    //    wyslijDoSerwera(Client.idCLIENT);
        this.setContentPane(new JLabel(new ImageIcon("src\\main\\resources\\siatka.png")));
        this.setSize(250, 222);
        this.setVisible(true);
        this.setResizable(false);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    public void wylaczPlansze() {
        this.setVisible(false);
    }

    public void wyslijDoSerwera(String wiadomosc) {
        try {
            dataOutputStream.writeUTF(wiadomosc);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void zaznaczRuchSerwera(int coordX, int coordY) {
        kwadraty[coordX][coordY].ustawKrzyzyk();
    }
}
