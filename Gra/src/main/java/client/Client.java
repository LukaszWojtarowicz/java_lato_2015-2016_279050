package client;

import database.GUIBase;
import serwer.Serwer;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

/**
 * Created by Łukasz on 2016-04-02.
 */

public class Client implements Runnable {

    private static final int PORT = 50010;
    private static final String HOST = "localhost";

    private Socket socket;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    public static String idCLIENT;
    private PlanszaGryClient planszaGryClient;

    public void run() {
        idCLIENT = GUIBase.name;
        createConnection();//bool po udanym lub ne polaczeniu +dodano zmiena w serwerze  boola ktorego ustawia cliennt
        planszaGryClient = new PlanszaGryClient(dataOutputStream);
        planszaGryClient.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                planszaGryClient.wyslijDoSerwera("exit");
                closeConnection();
                super.windowClosing(e);
            }
        });
        if (Serwer.polaczono == true) {
            try {
                String msgin = "";
                while (!msgin.equals("exit")) {//tu oifowac data exception
                    msgin = dataInputStream.readUTF();
                    System.out.println("Message from server:" + msgin);
                    serverMove(msgin);
                }
                closeConnection();
                planszaGryClient.wylaczPlansze();

            } catch (SocketException e) {
                JOptionPane.showMessageDialog(null, "Connection lost");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    else
    {
        JOptionPane.showMessageDialog(null, "Connection failed");
    }

}

    private void serverMove(String messageWithCoords) {
        try {
            String[] coords = messageWithCoords.split(" ");
            int coordX = Integer.parseInt(coords[0]);
            int coordY = Integer.parseInt(coords[1]);
            planszaGryClient.zaznaczRuchSerwera(coordX, coordY);
        } catch (NumberFormatException e) {
            //silent
        }
    }

    private void createConnection() {
        try {
            socket = new Socket(HOST, PORT);
            System.out.println("Client: Nawiazano polaczenie z serwerem: " + socket);
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataInputStream = new DataInputStream(socket.getInputStream());
            Serwer.polaczono=true;
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Nie mozna polaczyc ze serwerem");
            closeConnection();
            e.printStackTrace();
        }
    }

    private void closeConnection() {
        try {
            dataInputStream.close();
            dataOutputStream.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}