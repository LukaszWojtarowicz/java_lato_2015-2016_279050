package serwer;

import database.GUIBase;
import logika.Gracz;
import logika.Logika;
import model.Kwadrat;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.SocketException;


/**
 * Created by Łukasz on 2016-03-19.
 */
public class PlanszaGrySerwer extends JFrame {
    final Gracz graczSerwer = new Gracz("Gracz1");
    final Gracz graczKlient = new Gracz("Gracz2");
    final Logika logika;
    private final DataOutputStream dataOutputStream;
    private Kwadrat[][] kwadraty = new Kwadrat[3][3];


    public PlanszaGrySerwer(DataOutputStream dataOutputStream) {

        this.dataOutputStream = dataOutputStream;
        init();
        logika = new Logika();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                kwadraty[i][j] = new Kwadrat(i, j);
                final int finalJ = j;
                final int finalI = i;
                kwadraty[i][j].addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (kolejGraczSerwer()) {

                            logika.zajmijPoleGracz1(kwadraty[finalI][finalJ].getCoordX(), kwadraty[finalI][finalJ].getCoordY());
                            graczSerwer.ileRuchow++;
                            logika.ileruchow = graczSerwer.ileRuchow + graczKlient.ileRuchow;
                            graczSerwer.ruchUp();
                            graczKlient.ruchDown();
                            wyslijDoClienta(String.valueOf(finalI) + " " + String.valueOf(finalJ));
                            kwadraty[finalI][finalJ].ustawKrzyzyk();
                            logika.sprawdzStanGry();
                            if (logika.koniecGry) {
                                wylaczPlansze();
                                wyslijDoClienta("exit");
                            }
                          }

                        }

                });
                this.add(kwadraty[i][j]);
            }
        }
    }

    private void init() {
        this.setTitle("Gracz1 - Serwer");
        this.setContentPane(new JLabel(new ImageIcon("src\\main\\resources\\siatka.png")));
        this.setSize(250, 222);
        this.setVisible(true);
        this.setResizable(false);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    private boolean kolejGraczSerwer() {
        return graczSerwer.wykonanyRuch == 0;
    }

    private void wylaczPlansze() {
        this.setVisible(false);
    }

    public void zaznaczRuchClienta(int coordX, int coordY) {
            kwadraty[coordX][coordY].ustawKolko();
            logika.zajmijPoleGracz2(kwadraty[coordX][coordY].getCoordX(), kwadraty[coordX][coordY].getCoordY());
            graczKlient.ileRuchow++;
            logika.sprawdzStanGry();
            if (logika.koniecGry) wylaczPlansze();
            logika.ileruchow = graczSerwer.ileRuchow + graczKlient.ileRuchow;
            graczKlient.ruchUp();
            graczSerwer.ruchDown();
    }

    public void wyslijDoClienta(String wiadomosc) {
        try {
            dataOutputStream.writeUTF(wiadomosc);
        } catch (SocketException e){
            //silence
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
