package serwer;

import database.GUIBase;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Łukasz on 2016-04-02.
 */
public class Serwer implements Runnable {
    private static final int PORT = 50010;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    private ServerSocket serverSocket;
    private Socket socket;
    public static String idSERVER;
    //public static String idClentOdebrany;
    private PlanszaGrySerwer planszaGrySerwer;
    private AtomicBoolean closeGame = new AtomicBoolean();
   public  static boolean polaczono;

    public void run() {
        idSERVER = GUIBase.name ;
        createConnection();
        //getClientId();
        planszaGrySerwer= new PlanszaGrySerwer(dataOutputStream);
        planszaGrySerwer.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                planszaGrySerwer.wyslijDoClienta("exit");
                closeGame.set(true);
                closeConnection();
                super.windowClosing(e);
            }
        });

        try {
            String msgin = "";
            while (!closeGame.get() && !msgin.equals("exit")) {
                msgin = dataInputStream.readUTF();
                System.out.println("Message from client:" + msgin);
                clientMove(msgin);
            }
            closeConnection();
        } catch (SocketException e){
            JOptionPane.showMessageDialog(null, "Connection lost");
            closeConnection();
        } catch (IOException e) {
            closeConnection();
            e.printStackTrace();
        }
    }

    private void createConnection() {
        try {
            serverSocket = new ServerSocket(PORT);
            System.out.println("Serwer: Uruchomiono " + serverSocket);

            JOptionPane.showMessageDialog(null, "Czekaj na uzyskanie polaczenia z drugim przeciwnikiem....");

            socket = serverSocket.accept();
            System.out.println("Serwer:" + socket + " - Polaczono");

            dataInputStream = new DataInputStream(socket.getInputStream());
            dataOutputStream = new DataOutputStream(socket.getOutputStream());

        } catch (IOException e) {
            closeConnection();
            e.printStackTrace();
        }
    }

    private void clientMove(String messageWithCoords){
        try {
            String[] coords = messageWithCoords.split(" ");
            int coordX = Integer.parseInt(coords[0]);
            int coordY = Integer.parseInt(coords[1]);
            planszaGrySerwer.zaznaczRuchClienta(coordX,coordY);
        } catch (NumberFormatException e) {
            // silent
        }
    }

    private void closeConnection(){
        try {
            dataInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try{
            dataOutputStream.close();
        } catch (IOException ee) {
            ee.printStackTrace();
        }
        try{
            socket.close();
        } catch (IOException eee) {
            eee.printStackTrace();
        }
        try{
            serverSocket.close();
        } catch (IOException eeee) {
            eeee.printStackTrace();
        }
    }
 /*   public void getClientId(){
         try
            {
                idClentOdebrany=dataInputStream.readUTF();
         }catch (IOException ee) {
             closeConnection();
             ee.printStackTrace();
         }
       }*/
}


