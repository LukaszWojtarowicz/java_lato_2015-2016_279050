package chat;

import database.GUIBase;

import javax.swing.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Łukasz on 2016-04-10.
 */
public class ChatServer extends JFrame {
    static ServerSocket serverSocket;
    static Socket socket;
    static DataInputStream din;
    static DataOutputStream dout;
    private static JTextArea messageTextArea;
    private JTextField messageTextField;
    private JButton send;
    public static String idSERVER;

    public ChatServer() {
        createWindow();
        createConnection();
        String msgin ="";
        while(!msgin.equals("exit")){
            try {
                msgin = din.readUTF();
                messageTextArea.setText(messageTextArea.getText().trim() + "\nClient:"+" \t" + msgin); //set the incoming message in messageTextArea
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void createConnection() {
        try{
            serverSocket = new ServerSocket(50050);
            socket = serverSocket.accept();
            din = new DataInputStream(socket.getInputStream());
            dout = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createWindow() {
        idSERVER = GUIBase.name ;
        this.setTitle("Chat-server-"+ idSERVER);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);//ustawienie domyslnego zamykania okna
        this.setSize(350, 370);//ustawienie rozmiaru okna
        this.setResizable(false);
        this.setLayout(null);

        messageTextArea = new JTextArea();
      //  messageTextArea.setEditable(false);
        messageTextArea.setBounds(0, 0, 340, 300);
        messageTextArea.setVisible(true);

        this.add(messageTextArea);

        messageTextField = new JTextField();
        messageTextField.setBounds(10, 300, 250, 20);
        messageTextField.setVisible(true);
        this.add(messageTextField);

        send = new JButton("Send");
        send.setBounds(260, 300, 80, 20);
        send.setVisible(true);
        send.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendActionPerformed();
                messageTextField.setText("");
            }
        });
        this.add(send);
        this.setVisible(true);
    }

    private void sendActionPerformed() {
        try {
            String msgout;
            msgout = messageTextField.getText().trim();
            messageTextArea.setText(messageTextArea.getText().trim() + "\n"+ ChatServer.idSERVER +"\t"  + msgout );//set the outcoming message in messageTextArea
            dout.writeUTF(msgout);//wysylanie wiadomosci od servera

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}