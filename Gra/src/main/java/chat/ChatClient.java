package chat;

import database.GUIBase;

import javax.swing.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by Łukasz on 2016-04-10.
 */
public class ChatClient extends JFrame{
    static Socket socket;
    static DataInputStream din;
    static DataOutputStream dout;
    private static JTextArea messageTextArea;
    private JTextField messageTextField;
    private JButton sendButton;
    public static String idCLIENT;

    public ChatClient() {
        createWindow();
        createConnection();

        String msgin = "";
        while (!msgin.equals("exit")) {
            try {
                msgin = din.readUTF();
                messageTextArea.setText(messageTextArea.getText().trim() + "\nServer:"+" \t" + msgin);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void createConnection() {
        try {
            socket = new Socket("127.0.0.1", 50050);
            din = new DataInputStream(socket.getInputStream());
            dout = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Nie mozna polaczyc ze serwerem");
            e.printStackTrace();
        }
    }

    private void createWindow() {
        idCLIENT = GUIBase.name ;
        this.setTitle("Chat-client-"+idCLIENT);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);//ustawienie domyslnego zamykania okna
        this.setSize(350, 370);//ustawienie rozmiaru okna
        this.setResizable(false);
        this.setLayout(null);

        messageTextArea = new JTextArea();
        //  messageTextArea.setEditable(false);
        messageTextArea.setBounds(0, 0, 340, 300);
        messageTextArea.setVisible(true);
        this.add(messageTextArea);

        messageTextField = new JTextField();
        messageTextField.setBounds(10, 300, 250, 20);
        messageTextField.setVisible(true);
        this.add(messageTextField);

        sendButton = new JButton("Send");
        sendButton.setBounds(260, 300, 80, 20);
        sendButton.setVisible(true);
        sendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendActionPerformed();
                messageTextField.setText("");
            }
        });
        this.add(sendButton);
        this.setVisible(true);
    }

    private void sendActionPerformed() {
        try {
            String msgout;
            msgout = messageTextField.getText().trim();
            messageTextArea.setText(messageTextArea.getText().trim() + "\n"+ ChatClient.idCLIENT +"\t" + msgout);//set the outcoming message in messageTextArea
            dout.writeUTF(msgout);//wysylanie wiadomosci od servera

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

