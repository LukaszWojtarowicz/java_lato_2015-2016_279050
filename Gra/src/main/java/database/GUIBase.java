package database;



import javax.swing.*;
import java.io.IOException;


/**
 * Created by Łukasz on 2016-04-24.
 */


public class GUIBase extends JFrame {

    PostgreSQL baza=new PostgreSQL();
    DataBaseComunicats comunicats;
    private JLabel jlbPassword;
    private JTextField passwordField;
    private JLabel jlbLogin;
    private JTextField loginField;
    private JButton loginButton;
    public static String name;

    public GUIBase() {
        createWindow();

    }




    private void createWindow() {
        this.setTitle("Logowanie");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);//ustawienie domyslnego zamykania okna
        this.setSize(400, 200);//ustawienie rozmiaru okna
        this.setResizable(false);
        this.setLayout(null);


        jlbLogin = new JLabel("Enter the login: ");
        jlbLogin.setBounds(20, 20, 150, 20);
        jlbLogin.setVisible(true);
        this.add(jlbLogin);


        loginField = new JTextField();
        loginField.setBounds(180, 20, 150, 20);
        loginField.setVisible(true);
        this.add(loginField);

        jlbPassword = new JLabel("Enter the password: ");
        jlbPassword.setBounds(20, 50, 150, 20);
        jlbPassword.setVisible(true);
        this.add(jlbPassword);


        passwordField = new JTextField();
        passwordField.setBounds(180, 50, 150, 20);
        passwordField.setVisible(true);
        this.add(passwordField);


        loginButton = new JButton("LOGIN");
        loginButton.setBounds(150, 100, 100, 20);

        loginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginActionPerformed();
                closeLogin();
            }
        });
        loginButton.setVisible(true);

        this.add(loginButton);
        this.setVisible(true);
        try {
            comunicats = new DataBaseComunicats();
        } catch (IOException e) {
            e.printStackTrace();
        }
        comunicats.start();



    }


    private void loginActionPerformed() {
        name=loginField.getText();
        baza.login(loginField.getText(),passwordField.getText());
    }

    void closeLogin() {
        this.setVisible(false);
    }

}


