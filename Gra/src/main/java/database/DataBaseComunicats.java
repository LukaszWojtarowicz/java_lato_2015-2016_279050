package database;

import java.io.IOException;
import java.net.*;

/**
 * Created by Łukasz on 2016-04-24.
 */


public class DataBaseComunicats extends Thread {

    private DatagramSocket socket;
    private InetAddress serverIp;
    private boolean loginStatus = false;
    private boolean registerStatus = false;


    public DataBaseComunicats() throws IOException {

        try {
            this.socket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }

    }


    public void run() {

        while (true) {

            byte[] data = new byte[50];
            DatagramPacket packet = new DatagramPacket(data, data.length);


            try {

                socket.receive(packet);
                String message = new String(packet.getData());
                message = message.trim();
                if (message.equals("1")) loginStatus = true;
                if (message.equals("11")) registerStatus = true;

            } catch (IOException e) {
                System.out.print(e);
            }

        }
    }


    public void send(byte[] data, String adress) {

        try {
            if (adress != null) this.serverIp = InetAddress.getByName(adress);
            else {
                serverIp = serverIp.getLocalHost();
            }

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        DatagramPacket packet = new DatagramPacket(data, data.length, serverIp, 1331);


        try {

            socket.send(packet);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public boolean isLoginStatus() {

        return loginStatus;

    }


    public boolean isRegisterStatus() {

        return registerStatus;

    }

}

