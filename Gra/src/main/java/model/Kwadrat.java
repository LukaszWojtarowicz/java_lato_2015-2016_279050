package model;

import javax.swing.*;

/**
 * Created by Łukasz on 15.04.2016.
 */
public class Kwadrat extends JButton {

    private final int width = 50;
    private final int height = 50;

    private int coordX;
    private int coordY;

    public int getCoordX() {
        return coordX;
    }

    public int getCoordY() {
        return coordY;
    }

    public Kwadrat(int coordX, int coordY) {
        this.coordX = coordX;
        this.coordY = coordY;
        this.setIcon(new ImageIcon("src\\main\\resources\\pusty.png"));
        this.setBorder(null);
        this.setBounds(coordY * 80 + 14, coordX * 64 + 8, width, height);
    }

    public void ustawKolko() {

        this.setDisabledIcon(new ImageIcon("src\\main\\resources\\kolko.png"));
        this.setEnabled(false);
    }

    public void ustawKrzyzyk() {

        this.setDisabledIcon(new ImageIcon("src\\main\\resources\\krzyzyk.png"));
        this.setEnabled(false);
    }
}
