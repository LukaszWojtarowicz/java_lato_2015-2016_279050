package logi;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Łukasz  on 15.04.2016.
 */
public class Log {
    public static void zapisz(String string){
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\main\\resources\\game.logs.txt", true));
            bufferedWriter.append(string);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
