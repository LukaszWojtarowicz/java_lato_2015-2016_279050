package logika;

/**
 * Created by Łukasz on 2016-03-12.
 */
public class Gracz {
    private String imie;
    public int wykonanyRuch;
    public int ileRuchow;

    public Gracz(String imie) {
        this.imie = imie;
    }

    public void ruchUp() {
        wykonanyRuch = 1;
    }

    public void ruchDown() {
        wykonanyRuch = 0;
    }
}
