package logika;

import client.Client;
import client.PlanszaGryClient;
import logi.Log;
import menu.Audio;
import menu.GUIStart;
import serwer.PlanszaGrySerwer;
import serwer.Serwer;

import javax.swing.*;

/**
 * Created by Łukasz on 2016-03-10.
 */
public class Logika extends GUIStart {
    private final String WYGRALGRACZ1 = "Wygral gracz 1";
    private final String WYGRALGRACZ2 = "Wygral gracz 2";
    public int[][] plansza = new int[3][3];
    public boolean koniecGry = false;
    public int ileruchow = 0;



    Audio muzyka2;
    String audioPath2;
    public Logika() {
        plansza = new int[3][3]; // inicjacja tablicy
        // uzupełnienie tablicy na poczatku zerami;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                plansza[i][j] = 0;
            }
        }


        muzyka2= new Audio();
        audioPath2 =  "src\\main\\resources\\win.wav";

    }


    public void sprawdzStanGry() {
        if ((plansza[0][0] + plansza[0][1] + plansza[0][2]) == 3 ||
                (plansza[1][0] + plansza[1][1] + plansza[1][2]) == 3 ||
                (plansza[2][0] + plansza[2][1] + plansza[2][2]) == 3 ||
                (plansza[0][0] + plansza[1][0] + plansza[2][0]) == 3 ||
                (plansza[0][1] + plansza[1][1] + plansza[2][1]) == 3 ||
                (plansza[0][2] + plansza[1][2] + plansza[2][2]) == 3 ||
                (plansza[0][0] + plansza[1][1] + plansza[2][2]) == 3 ||
                (plansza[0][2] + plansza[1][1] + plansza[2][0]) == 3) {
           // muzyka.stop();
            muzyka2.play(audioPath2);
            JOptionPane.showMessageDialog(null, WYGRALGRACZ1 );
            Log.zapisz(WYGRALGRACZ1 + " wykonano:" + (ileruchow + 1) + " ruchow\n");
            koniecGry = true;
        } else if ((plansza[0][0] + plansza[0][1] + plansza[0][2]) == 21 ||
                (plansza[1][0] + plansza[1][1] + plansza[1][2]) == 21 ||
                (plansza[2][0] + plansza[2][1] + plansza[2][2]) == 21 ||
                (plansza[0][0] + plansza[1][0] + plansza[2][0]) == 21 ||
                (plansza[0][1] + plansza[1][1] + plansza[2][1]) == 21 ||
                (plansza[0][2] + plansza[1][2] + plansza[2][2]) == 21 ||
                (plansza[0][0] + plansza[1][1] + plansza[2][2]) == 21 ||
                (plansza[0][2] + plansza[1][1] + plansza[2][0]) == 21) {

            muzyka2.play(audioPath2);
            JOptionPane.showMessageDialog(null,WYGRALGRACZ2);
            Log.zapisz(WYGRALGRACZ2 +" wykonano:" + (ileruchow + 1) + " ruchow\n");

            koniecGry = true;
        } else if (ileruchow > 7)//liczy od 0-8 czyli 9 ruchow
        {

            JOptionPane.showMessageDialog(null, "Zajeto cala plansze- remis");
            Log.zapisz("Remis\n");
            koniecGry = true;
        }


    }

    public void zajmijPoleGracz1(int coordX, int coordY) {
        plansza[coordX][coordY] = 1;
    }
    public void zajmijPoleGracz2(int coordX, int coordY) {
        plansza[coordX][coordY] = 7;
    }
}


