package menu;

import client.Client;
import serwer.Serwer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Created by Łukasz on 2016-03-13.
 */
public class GUIHostJoin extends JFrame {
    private JButton hostButton;
    private JButton joinButton;

    public GUIHostJoin() {

        super("Host/Join server");
        GridBagLayout gridBag = new GridBagLayout();
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.CENTER;
        gridBag.setConstraints(this, constraints);
        setLayout(gridBag);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(400, 300);
        hostButton = new JButton("Host");
        add(hostButton);
        hostButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Thread thread = new Thread(new Serwer());
                thread.start();
                closeHost();
            }
        });

        joinButton = new JButton("Join");
        joinButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Thread thread = new Thread(new Client());
                thread.start();
                closeHost();
            }
        });
        add(joinButton);
    }

    void closeHost() {
        this.setVisible(false);
    }

    void openHost() {
        this.setVisible(true);
    }
}
