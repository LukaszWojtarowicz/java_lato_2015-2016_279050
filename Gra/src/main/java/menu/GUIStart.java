package menu;

import chat.ChatClient;
import database.GUIBase;
import menu.Audio;
import javax.swing.*;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.IOException;

/**
 * Created by Łukasz on 2016-03-04.
 */
//klasa odpowiadajaca za interfejs

public class GUIStart extends JFrame {

    private JButton nowyButton;
    private JButton wynikiButton;
    private JButton czatButton;
    private JButton loginButton;
    private JButton koniecButton;

    private JLabel obrazekLable;


    Audio muzyka;
    String audioPath;

    //konstruktor klasy-okna
    public GUIStart() {

        createWindow();

    }

    private void createWindow() {
        this.setTitle("Kolko i krzyzyk");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);//ustawienie domyslnego zamykania okna
        this.setSize(400, 400);//ustawienie rozmiaru okna
        this.setContentPane(new JLabel(new ImageIcon("src\\main\\resources\\pulpit.png")));
        this.setResizable(false);
        this.setLayout(null);

        muzyka = new Audio();
        audioPath = "src\\main\\resources\\podklad.wav";
        muzyka.play(audioPath);

        //tworzenie klawiszy i ich funkcji

        loginButton = new JButton("Login");
        loginButton.setBounds(150, 15, 100, 50);
        loginButton.setVisible(true);
        loginButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new GUIBase();

            }
        });
        this.add(loginButton);


        nowyButton = new JButton("Nowa Gra");
        nowyButton.setBounds(150,75,100,50);
        nowyButton.setVisible(true);
        nowyButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                GUIHostJoin okno = new  GUIHostJoin();
                okno.openHost();

            }

        });
        this.add(nowyButton);


        wynikiButton = new JButton("Wyniki");
        wynikiButton.setBounds(150,135,100,50);
        wynikiButton.setVisible(true);
        wynikiButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try { //tu jest try catch bo w tej klasie jest obsluga pliku to do bledu otwarcia jest potrzebny
                    new GUIWynik();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        });
        this.add(wynikiButton);


        czatButton = new JButton("Czat");
        czatButton.setBounds(150,195,100,50);
        czatButton.setVisible(true);
        czatButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GUIChatHostJoin okno2=  new GUIChatHostJoin();
                okno2.openHostJoin();
            }
        });
        this.add(czatButton);



        koniecButton = new JButton("Koniec");
        koniecButton.setBounds(150,255,100,50);
        koniecButton.setVisible(true);
        koniecButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                muzyka.stop();
                close();
            }
        });
        this.add(koniecButton);

    this.setVisible(true);


    }

    void close() {
        this.setVisible(false);
    }


}





















































/////



