package menu;

import javax.swing.*;
import java.io.*;

/**
 * Created by Łukasz on 2016-04-02.
 */
public class GUIWynik extends  JFrame{

    private JTextArea textArea;
    private JScrollPane scrollPane;


    GUIWynik() throws IOException {
        this.setTitle("Wyniki");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);//ustawienie domyslnego zamykania okna
        this.setSize(350,350);//ustawienie rozmiaru okna
        this.setVisible(true);

        textArea =new JTextArea();
        textArea.setSize(340,340);
        textArea.setEditable(false);

        File plik= new File(("src\\main\\resources\\game.logs.txt"));
            BufferedReader in = new BufferedReader(new FileReader(plik));
            String line = in.readLine();
            while (line != null) {
                textArea.append(line + "\n");
                line = in.readLine();
            }
        textArea.setVisible(true);
        this.add(textArea);

        scrollPane = new JScrollPane (textArea,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        this.add(scrollPane);
    }
}


