package menu;

import chat.ChatClient;
import chat.ChatServer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Łukasz on 2016-04-10.
 */
public class GUIChatHostJoin extends JFrame {
    private JButton host2Button;
    private JButton join2Button;

    public GUIChatHostJoin() {
        super("Chat Host/Join");
        GridBagLayout gridBag = new GridBagLayout();
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.CENTER;
        gridBag.setConstraints(this, constraints);
        setLayout(gridBag);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(400, 300);

        host2Button = new JButton("Host");
        host2Button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeHostJoin();
                Thread thread = new Thread(new Runnable() {
                    public void run() {
                        new ChatServer();
                    }
                });
                thread.start();
            }
        });
        add(host2Button);

        join2Button = new JButton("Join");
        join2Button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeHostJoin();
                Thread thread = new Thread(new Runnable() {
                    public void run() {
                        new ChatClient();
                    }
                });
                thread.start();
            }
        });
        add(join2Button);
    }

    void closeHostJoin() {
        this.setVisible(false);
    }

    void openHostJoin() {
        this.setVisible(true);
    }

}